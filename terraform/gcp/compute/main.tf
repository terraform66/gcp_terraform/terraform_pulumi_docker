# Specify the provider (GCP, AWS, Azure)
provider "google"{
  credentials = file("~/.config/gcloud/cicd_gcp_creds.json")
  project = var.project_name
  region = "europe-central2-a"
}

data "google_compute_network" "default" {
  name = "default"
}

resource "google_compute_firewall" "http-5000" {
  name    = "http-5000"
  network = data.google_compute_network.default.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = [var.port_number]
  }
}

resource "google_compute_instance" "infra" {
  name = "infra-as-code-instance"
  machine_type = "g1-small"
  zone = "us-east1-b"
  tags =[
    "name","infra-as-code"
    //need to add new one
  ]

  boot_disk {
    auto_delete = true
    initialize_params {
      image = var.boot_image_name
      type = "pd-standard"
    }
  }

  metadata = {
    gce-container-declaration = var.docker
  }

  labels = {
    container-vm = "debian-9"
  }

  network_interface {
    network = "default"
    access_config {
      // Ephemeral IP
    }
  }
}