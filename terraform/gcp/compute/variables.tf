variable "project_name" {
  type = string
  default = "ci-cd"
}

#Docker
variable "port_number" {
  type = string
  default = "5000"
}

variable "docker" {
  type = string
  # Change the image: string to match the docker image you want to use
  default = "spec:\n  containers:\n    - name: test-docker\n      image: 'andreichenko/infra-as-code:latest'\n      stdin: false\n      tty: false\n  restartPolicy: Always\n"
}

variable "boot_image_name" {
  type = string
  default = "debian-9"
}